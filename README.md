# Hash TCP Server

## Dependencies

Following ubuntu packages required:

```
build-essential
cmake
git
libboost-all-dev
googletest
libgtest-dev
python3
```

## Getting started

Clone repository, create build directory and run cmake from it:

```
git clone https://gitlab.com/rglory/hash-tcp-server.git
cd hash-tcp-server
mkdir build
cd build
cmake ..
make
```

## Run test

```
make tests
```
Or execute programs from build/test subdirectory

### Run server

From build directory:
```
./src/hashserver -p 1026 -H md5 -t 4
```

> -p 1026
Port to listen

> -H md5 or crc32
Hash to produce

> -t 4
Threads in thread poool (0 for direct computation from boost io thread without pool)

### Run client

```
echo "Hello world" | nc localhost 1026
F0EF7081E1539AC00EF5B761B4FB01B3
```

Server output:
```
received connection from 127.0.0.1:44930
connection with 127.0.0.1:44930 terminated 1 hashes produced
```

### Stress test

Start server and run `stress.py` from test subdirectory:

```
usage: stress.py [-h] -p PORT [-m MESSAGES] [-c CONNECTIONS] [-s SIZE]
```

> -p port
Port where server is listening to

> -m messages
Amount of messages to send per connection (default 1024)

> -c connections
Amount of connections (default 4)

> -s size
Size of a single message to hash (default 10240)

Example md5:
```
./test/stress.py -p 1026
Sending 1024 messages over 4 connections with message size 10240
Duration: 0.7718689441680908 messages/second 5306.60033798174 kbytes/sec 53066.003379817404
```

Example crc32:
```
Sending 1024 messages over 4 connections with message size 10240
Duration: 0.6438062191009521 messages/second 6362.16283483544 kbytes/sec 63621.628348354396
```
