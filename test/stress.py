#!/bin/env python3

import asyncio, time, argparse, sys

buffer = None
port = None
messages = 0;

async def client():
    reader, writer = await asyncio.open_connection( 'localhost', port )

    for i in range( messages ):
        writer.write( buffer )
        data = await reader.readline()
        #print( "data:", data.decode() )

    writer.close()
    await writer.wait_closed()

async def main(connections):
    coroutines = [ client() for i in range(connections) ]
    await asyncio.gather( *coroutines )

parser = argparse.ArgumentParser(description="Stress test")
parser.add_argument( '-p', '--port', type=int, help='tcp port', required=True )
parser.add_argument( '-m', '--messages', type=int, help='amount of messages', default=1024 )
parser.add_argument( '-c', '--connections', type=int, help='connections', default=4 )
parser.add_argument( '-s', '--size', type=int, help='connections', default=10240 )

args = parser.parse_args()

connections = args.connections
size        = args.size
messages    = args.messages
port        = args.port

buffer = ('a' * size + '\n' ).encode();
print( "Sending", messages, "messages over", connections, "connections with message size", size )
start = time.time()
asyncio.run( main(connections) )
duration = time.time() - start


print( "Duration:", duration, "messages/second", connections * messages / duration, "kbytes/sec", connections * messages * size / (1024 * duration) )


