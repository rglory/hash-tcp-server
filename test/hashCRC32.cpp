#include <gtest/gtest.h>
#include <HashCRC32.hpp>

TEST(HashCRC32,Test)
{
    CRC32processor proc;
    auto test = [&proc]( const std::string &str ) {
        proc.process( str.c_str(), str.size() );
        return proc.finalize();
    };

    EXPECT_EQ( "00000000\n", test( "" ) );
    EXPECT_EQ( "B739E0D5\n", test( "Hello world\n" ) );
    EXPECT_EQ( "00000000\n", test( "" ) );
    EXPECT_EQ( "B739E0D5\n", test( "Hello world\n" ) );
}
