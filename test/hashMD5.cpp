#include <gtest/gtest.h>
#include <HashMD5.hpp>

TEST(HashMD5,Test)
{
    MD5processor proc;
    auto test = [&proc]( const std::string &str ) {
        proc.process( str.c_str(), str.size() );
        return proc.finalize();
    };

    EXPECT_EQ( "D41D8CD98F00B204E9800998ECF8427E\n", test( "" ) );
    EXPECT_EQ( "F0EF7081E1539AC00EF5B761B4FB01B3\n", test( "Hello world\n" ) );
    EXPECT_EQ( "D41D8CD98F00B204E9800998ECF8427E\n", test( "" ) );
    EXPECT_EQ( "F0EF7081E1539AC00EF5B761B4FB01B3\n", test( "Hello world\n" ) );
}
