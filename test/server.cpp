#include <gtest/gtest.h>
#include <Server.hpp>
#include <HashCRC32.hpp>

std::string test;

void onHash( std::string str )
{
    test = std::move( str );
}

void processString( Server &server, const std::string &str )
{
    std::copy( str.begin(), str.end(), server.buffer() );
    server.populated( str.size() );
    server.onData();
}

TEST(Server,CRC32)
{
    Server server( "crc32", onHash );
    processString( server, "Hello world\n" );
    EXPECT_EQ( "B739E0D5\n", test );
}

TEST(Server,MD5)
{
    Server server( "md5", onHash );
    processString( server, "Hello world\n" );
    EXPECT_EQ( "F0EF7081E1539AC00EF5B761B4FB01B3\n", test );
}