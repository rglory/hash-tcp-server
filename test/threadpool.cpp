#include <gtest/gtest.h>
#include <ThreadPool.hpp>
#include <chrono>

TEST(ThreadPool,Run)
{
    std::vector<std::string> test = { "looong string", "string1", "string2"  }; // has to be sorted !
    std::vector<std::string> r;
    std::mutex m;
    ThreadPool<std::string> pool( 2, [&r,&m]( std::string str ) { std::scoped_lock lock( m ); r.push_back( std::move( str ) ); } );
    for( const auto &s : test )
        pool.push( s );
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_EQ( test.size(), r.size() );
    std::sort( r.begin(), r.end() );
    for( size_t i = 0; i < test.size(); ++i )
        EXPECT_EQ( test[i], r[i] );

}
