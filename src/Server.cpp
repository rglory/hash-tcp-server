#include "Server.hpp"

std::unique_ptr<ServerThreadPool> Server::theThreadPool;

void Server::startPool( int threads )
{
    theThreadPool = std::make_unique<ServerThreadPool>( threads, []( ServerPtr data ) { data->onData(); } );
}

Server::Server( const std::string &hash, HashProducedCalleback cb ) :
    m_proc( HashProcessorFactory::instance().create( hash ) ),
    m_cb( std::move( cb ) )
{
}

Server::Server( ip::tcp::socket sock, ip::tcp::endpoint endp, const std::string &hash ) :
    m_sock( std::make_unique<decltype(sock)>( std::move( sock ) ) ),
    m_endp( std::make_unique<decltype(endp)>( std::move( endp ) ) ),
    m_proc( HashProcessorFactory::instance().create( hash ) )
{
    std::cout << "received connection from " << *m_endp << std::endl;
}



void Server::onData()
{
    auto b = m_buffer;
    const auto e = b + m_size;
    m_size = 0;

    for( auto p = b;; ) {
        if( p == e ) {
            if( b != p )
                m_proc->process( b, std::distance( b, p ) );
            break;
        }
            
        char c = *p++;
        if( c == '\n' ) {
            m_proc->process( b, std::distance( b, p ) );
            b = p;
            auto hash = m_proc->finalize();
            if( m_sock )
                m_sock->send( boost::asio::buffer( std::move( hash ) ) );
            else
                m_cb( std::move( hash ) );

            ++m_counter;
        }
    }
    startAsyncRead();    
} 



void Server::readHandler( const boost::system::error_code &code, size_t size ) 
{
    if( code ) {
        std::cout << "connection with " << *m_endp << " terminated " << m_counter << " hashes produced" << std::endl;
        return;
    }
    populated( size );
    if( theThreadPool ) 
        theThreadPool->push( shared_from_this() );
    else
        onData();
}

void Server::startAsyncRead()
{
    if( not m_sock )
        return;
    auto buffer = boost::asio::buffer( m_buffer, c_bufferSize );
    m_sock->async_read_some( buffer, [ptr=shared_from_this()] ( const boost::system::error_code &code, size_t size ) mutable { ptr->readHandler( code, size ); } );
}

