#include "HashMD5.hpp"

#include <boost/algorithm/hex.hpp>

namespace {
HashRegistartor<MD5processor> reg( "md5" );
}

 void MD5processor::process( const char *p, size_t size )
 {
    m_hash.process_bytes( p, size );
 }

 std::vector<char> MD5processor::finalize_()
 {
    md5::digest_type digest;
    m_hash.get_digest( digest );
    m_hash = decltype( m_hash ){};

    std::vector<char> r;
    for( auto v : digest ) {
        auto rv = htobe32( v );
        auto b = reinterpret_cast<const char *>( &rv );
        r.insert( r.end(), b, b + sizeof( rv ) );
    }
    return r;
 }
