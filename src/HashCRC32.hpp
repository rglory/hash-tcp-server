#ifndef HASHCRC32_HPP
#define HASHCRC32_HPP

#include "HashProcessor.hpp"

#include <boost/crc.hpp>

class CRC32processor : public HashProcessor {
public:
    virtual ~CRC32processor() = default;

    virtual void process( const char *p, size_t size ) override;

private:
    virtual std::vector<char> finalize_() override;

    boost::crc_32_type m_hash;    
};

#endif // HASHMD5_HPP

