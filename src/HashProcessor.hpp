#ifndef HASHPROCESSOR_HPP
#define HASHPROCESSOR_HPP

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>
#include <functional>

class HashProcessor {
public:
    virtual ~HashProcessor() = default;

    virtual void process( const char *p, size_t size ) = 0;
    std::string finalize();

private:
    virtual std::vector<char> finalize_() = 0;

};

using HashProcessorPtr = std::unique_ptr<HashProcessor>;

class HashProcessorFactory {
public:
    using Creator = std::function<HashProcessorPtr()>;
    static HashProcessorFactory &instance();

    void registerCreator( std::string name, Creator c );

    HashProcessorPtr create( const std::string &name );

    std::vector<std::string> names() const;

private:
    HashProcessorFactory() {}
    std::unordered_map<std::string, Creator> m_creators;
};


template<typename T>
class HashRegistartor {
public:
    HashRegistartor( std::string name ) 
    { 
        HashProcessorFactory::instance().registerCreator( std::move( name ), [] { return std::make_unique<T>(); } ); 
    }
};

#endif // HASHPROCESSOR_HPP