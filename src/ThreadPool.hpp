#ifndef THREADPOOL_HPP
#define THREADPOOL_HPP

#include <functional>
#include <deque>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <iostream>

template<typename T>
class ThreadPool {
public:
    using Processor = std::function<void( T )>;

    ThreadPool( size_t threads, Processor p );
    ~ThreadPool();

    void push( T t );

    void stop();

private:
    void process();

    Processor                 m_proc;
    std::deque<T>             m_data;
    bool                      m_stopped;
    std::vector<std::thread>  m_pool;
    std::mutex                m_mutex;
    std::condition_variable   m_var;
};

template<typename T>
ThreadPool<T>::ThreadPool( size_t threads, Processor p ) :
    m_proc( std::move( p ) ),
    m_stopped( false )
{
    while( threads-- ) m_pool.emplace_back( &ThreadPool<T>::process, this );
}

template<typename T>
ThreadPool<T>::~ThreadPool()
{
    stop();
}

template<typename T>
void ThreadPool<T>::stop()
{
    {
        std::scoped_lock<std::mutex> lock( m_mutex );
        if( m_stopped ) return;
        m_stopped = true;
        m_var.notify_all();
    }
    for( auto &th : m_pool )
        th.join();
}

template<typename T>
void ThreadPool<T>::push( T t )
{
    std::scoped_lock<std::mutex> lock( m_mutex );
    m_data.push_back( std::move( t ) );
    if( m_data.size() == 1 ) 
        m_var.notify_one();
}

template<typename T>
void ThreadPool<T>::process()
{
    std::unique_lock<std::mutex> lock( m_mutex );
    while( not m_stopped ) {
        if( m_data.empty() ) {
            m_var.wait( lock );
            continue;
        }
        T t = std::move( m_data.front() );
        m_data.pop_front();

        lock.unlock();
        try {
            m_proc( std::move( t ) );
        }
        catch( const std::exception &ex ) {
            std::cerr << "exception on processing data:" << ex.what() << std::endl;
        }
        lock.lock();
    }
}

#endif // THREADPOOL_HPP