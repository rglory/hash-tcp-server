#include "HashProcessor.hpp"

#include <stdexcept>

HashProcessorFactory &HashProcessorFactory::instance()
{
    static HashProcessorFactory instance;
    return instance;
}

std::string HashProcessor::finalize()
{
    auto v = finalize_();
    std::string r;
    for( auto c : v ) {
        char buffer[4];
        std::sprintf( buffer, "%02X", (unsigned char)c );
        r += std::string( buffer, 2 );
    }
    r += '\n';
    return r;
}

void HashProcessorFactory::registerCreator( std::string name, Creator c )
{
    m_creators.emplace( std::move( name ), std::move( c ) );
}

HashProcessorPtr HashProcessorFactory::create( const std::string &name )
{
    auto it = m_creators.find( name );
    if( it == m_creators.end() )
        throw std::runtime_error( "Cannot create hash processor for " + name + " - not registered" );
    return it->second();
}

std::vector<std::string> HashProcessorFactory::names() const
{
    std::vector<std::string> r;
    for( const auto &[name,c] : m_creators )
        r.push_back( name );
    std::sort( r.begin(), r.end() );
    return r;
}
