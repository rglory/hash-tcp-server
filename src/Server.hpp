#ifndef SERVER_HPP
#define SERVER_HPP

#include <boost/asio.hpp>

#include "ThreadPool.hpp"
#include "HashProcessor.hpp"

namespace ip = boost::asio::ip;

class Server;

using ServerPtr        = std::shared_ptr<Server>;
using ServerThreadPool = ThreadPool<ServerPtr>;

class Server : public std::enable_shared_from_this<Server> {
public:
    using HashProducedCalleback = std::function<void(std::string s)>;

    constexpr static size_t c_bufferSize = 512;
    
    Server( const std::string &hash, HashProducedCalleback cb );

    Server( ip::tcp::socket sock, ip::tcp::endpoint endp, const std::string &hash );

    char *buffer() { return m_buffer; }
    size_t bufferSize() const { return c_bufferSize; }

    void populated( size_t size ) { m_size = size; }

    void onData();

    void startAsyncRead();

    static void startPool( int threads );

private:
    std::unique_ptr<ip::tcp::socket>   m_sock;
    std::unique_ptr<ip::tcp::endpoint> m_endp;
    HashProcessorPtr                   m_proc;
    char                               m_buffer[c_bufferSize];
    int                                m_counter = 0;
    size_t                             m_size    = 0;
    HashProducedCalleback              m_cb;

private:
    void readHandler( const boost::system::error_code &code, size_t size );

    static std::unique_ptr<ServerThreadPool> theThreadPool;
};


#endif // SERVER_HPP