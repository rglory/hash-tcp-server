#ifndef HASHMD5_HPP
#define HASHMD5_HPP

#include "HashProcessor.hpp"

#include <boost/uuid/detail/md5.hpp>

using boost::uuids::detail::md5;

class MD5processor : public HashProcessor {
public:
    virtual ~MD5processor() = default;

    virtual void process( const char *p, size_t size ) override;

private:
    virtual std::vector<char> finalize_() override;

    md5 m_hash;    
};

#endif // HASHMD5_HPP

