#include "HashCRC32.hpp"

namespace {
HashRegistartor<CRC32processor> reg( "crc32" );
}

 void CRC32processor::process( const char *p, size_t size )
 {
    m_hash.process_bytes( p, size );
 }

 std::vector<char> CRC32processor::finalize_()
 {
    auto hash = htobe32( m_hash.checksum() );
    auto b = reinterpret_cast<const char *>( &hash );
    m_hash.reset();
    return std::vector<char>( b, b + sizeof( hash ) );
 }
