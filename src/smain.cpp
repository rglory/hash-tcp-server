#include <iostream>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

#include "Server.hpp"

namespace po = boost::program_options;

int main( int ac, const char **av )
{
    po::options_description desc( "Allowed options" );
    desc.add_options()
        ( "help,h", "produce help message" )
        ( "port,p",    po::value<uint16_t>()->required(),    "TCP port to listen" )
        ( "threads,t", po::value<int>()->default_value(0),   "thread pool size" )
        ( "hash,H",    po::value<std::string>()->required(), "hash type" );

    po::variables_map vm;
    po::store( po::parse_command_line( ac, av, desc ), vm );
    
    if( vm.count( "help" ) ) {
        std::cout << desc << "available hash types:";
        auto hs = HashProcessorFactory::instance().names();
        for( const auto &name : hs )
            std::cout << name << ' ';
        std::cout << std::endl;
        return 100;
    }

    try {
        po::notify( vm );
    }
    catch( const std::exception &ex ) {
        std::cerr << "error processing arguments: " << ex.what() << std::endl;
        std::cerr << desc << std::endl;
        return 101;
    }

    try {
        auto threads = vm["threads"].as<int>();
        if( threads > 0 ) 
            Server::startPool( threads );
        boost::asio::io_service ioService;
        auto work = std::make_unique<boost::asio::io_service::work>( ioService );
        std::thread thread( [&ioService]{ ioService.run(); } );
        ip::tcp::acceptor acceptor( ioService, ip::tcp::endpoint( ip::tcp::v4(), vm["port"].as<uint16_t>() ) );
        while( true ) {
            ip::tcp::socket sock( ioService );
            ip::tcp::endpoint endp;
            acceptor.accept( sock, endp );
            auto server = std::make_shared<Server>( std::move( sock ), std::move( endp ), vm["hash"].as<std::string>() );
            server->startAsyncRead();
        }
    }
    catch( const std::exception &ex )
    {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 102;
    }

}